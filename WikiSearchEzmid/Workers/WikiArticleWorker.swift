//
//  WikiArticleWorker.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import Alamofire

struct WikiArticleWorker {
    private let service = NetworkingService()
    
    func getArticles(query: ArticlesQueryAPI, _ completion: @escaping (_ result: Result<ArticleSearchAPI>) -> Void) {
        service.fetchItem(using: WikiArticleRouter.queryArticles(query: query), ofType: ArticleSearchAPI.self) { response in
            switch response {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                print("Get Articles error \(error)")
                completion(.failure(error))
            }
        }
    }
    
    func getArticle(withPageId pageId: Int, _ completion: @escaping (_ article: Result<ArticleDetailAPI>) -> Void) {
        service.fetchItem(using: WikiArticleRouter.queryArticlePage(pageId: pageId), ofType: ArticleDetailAPI.self) { response in
            switch response {
            case .success(let article):
                completion(.success(article))
            case .failure(let error):
                print("Get Article error \(error)")
                completion(.failure(error))
            }
        }
    }
}
