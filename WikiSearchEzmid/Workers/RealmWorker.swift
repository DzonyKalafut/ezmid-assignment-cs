//
//  RealmWorker.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 15/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmWorkerDelegate: class {
    func realmWorkerHasChanged()
}

class RealmWorker {
    private var realm: Realm!
    var notificationToken: NotificationToken?
    private var delegates = [RealmWorkerDelegate]()
    
    // MARK: Object lifecycle
    
    static let shared = RealmWorker()
    
    private init() {
        configure()
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    private func configure() {
        realm = try! Realm()
        observe()
    }
    
    // MARK: Observer delegates
    
    func addDelegate(delegate: RealmWorkerDelegate) {
        if indexOfDelegate(delegate: delegate) == nil {
            delegates.append(delegate)
        }
    }
    
    func removeDelegate(delegate: RealmWorkerDelegate) {
        if let index = indexOfDelegate(delegate: delegate) {
            delegates.remove(at: index)
        }
    }
    
    private func indexOfDelegate(delegate: RealmWorkerDelegate) -> Int? {
        return delegates.firstIndex(where: { $0 === delegate })
    }
    
    private func observe() {
        notificationToken = realm.observe({ (notification, realm) in
            for delegate in self.delegates {
                delegate.realmWorkerHasChanged()
            }
        })
    }
    
    // MARK: Operations
    
    func save(object: Object, update: Bool = false) -> Error? {
        do {
            try realm.write {
                realm.create(type(of: object), value: object, update: update)
            }
            return nil
        } catch {
            print("Object save error \(error)")
            return error
        }
    }
    
    
    func remove(object: Object) -> Error? {
        do {
            try realm.write {
                realm.delete(object)
            }
            return nil
        } catch {
            print("Object deletion error \(error)")
            return error
        }
    }
    
    func fetchObjects<T: Object>(ofType type: T.Type, sortedBy keyPath: String? = nil, ascending: Bool = true) -> [T]? {
        let result = realm.objects(type)
        if let keyPath = keyPath {
            return Array(result.sorted(byKeyPath: keyPath, ascending: ascending))
        }
        return Array(result)
    }
}
