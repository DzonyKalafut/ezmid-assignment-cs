//
//  PopoverRenderer+Extension.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 15/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationBannerSwift

protocol ErrorPopoverRenderer {
    func display(error: Error)
    func display(errorMessage: String)
}

protocol InfoPopoverRenderer {
    func display(message: String)
    func display(successMessage: String)
}

protocol PopoverRenderer: ErrorPopoverRenderer, InfoPopoverRenderer {}

extension ErrorPopoverRenderer where Self: UIViewController {
    func display(error: Error) {
        SVProgressHUD.dismiss()
        
        // ignore no connection errors
        guard (error as NSError).code != -1009 else {
            return
        }
        
        let banner = NotificationBanner(title: "errorTitleGeneral".localized, subtitle: error.localizedDescription, style: .danger)
        banner.show()
    }
    
    func display(errorMessage: String) {
        SVProgressHUD.dismiss()
        
        let banner = NotificationBanner(title: "errorTitleGeneral".localized, subtitle: errorMessage, style: .danger)
        banner.show()
    }
}

extension InfoPopoverRenderer where Self: UIViewController {
    func display(message: String) {
        SVProgressHUD.dismiss()
        
        let banner = NotificationBanner(title: "alertTitleInfo".localized, subtitle: message, style: .info)
        banner.show()
        
    }
    
    func display(successMessage: String) {
        SVProgressHUD.dismiss()
        
        let banner = NotificationBanner(title: "alertTitleSuccess".localized, subtitle: successMessage, style: .success)
        banner.show()
        
    }
}
