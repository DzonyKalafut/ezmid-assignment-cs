//
//  ActivityIindicator+Extension.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 15/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import SVProgressHUD

protocol ActivityIndicatorRenderer {
    func showActivityIndicator()
    func hideActivityIndicator()
}

extension ActivityIndicatorRenderer where Self: UIViewController {
    func showActivityIndicator() {
        SVProgressHUD.show()
    }
    
    func hideActivityIndicator() {
        SVProgressHUD.dismiss()
    }
}
