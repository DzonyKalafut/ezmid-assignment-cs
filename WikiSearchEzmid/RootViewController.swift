//
//  RootViewController.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 15/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import UIKit

class RootViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let firstVC = getArticleListViewController(type: .search)
        firstVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        
        let secondVC = getArticleListViewController(type: .saved)
        secondVC.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 1)
        
        viewControllers = [firstVC, secondVC]
    }
    
    private func getArticleListViewController(type: ArticleList.DisplayType) -> UIViewController {
        guard let nc = UIStoryboard(name: "ArticleList", bundle: nil).instantiateInitialViewController() as? UINavigationController else {
            fatalError("Failed to instantiate controller from ArticleList storyboard")
        }
        
        guard let vc = nc.viewControllers[0] as? ArticleListViewController else {
            fatalError("Failed to instantiate ArticleList viewController")
        }
        
        vc.interactor?.displayType = type
        
        return nc
    }
}
