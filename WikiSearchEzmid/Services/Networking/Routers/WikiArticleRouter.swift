//
//  WikiArticleRouter.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import Alamofire

enum WikiArticleRouter: URLRequestConfigurable {
    case queryArticles(query: ArticlesQueryAPI)
    case queryArticlePage(pageId: Int)
    
    var urlRequestConfiguration: URLRequestConfiguration {
        let url = Config.apiUrl
        var parameters: [String: AnyHashable]? = nil
        
        switch self {
        case .queryArticles(let query):
            parameters = [
                "action": "query",
                "list": "search",
                "srsearch": query.searchTerm.URLEscaped,
                "srlimit": query.limit,
                "sroffset": query.offset, "format": "json"
            ]
        case .queryArticlePage(let pageId):
            parameters = [
                "action": "parse",
                "pageid": pageId,
                "format": "json"
            ]
        }
        
        return URLRequestConfiguration(
            url: url,
            parameters: parameters
        )
    }
}
