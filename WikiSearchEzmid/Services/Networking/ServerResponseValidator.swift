//
//  ServerResponseValidator.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import Alamofire

private let ServerResponseValidation: DataRequest.Validation = { request, response, data in
    if response.statusCode >= 300 {
        return .failure(ServerError.failure(code: response.statusCode))
    } else {
        return .success
    }
}

extension DataRequest {
    func validateServerResponse() -> Self {
        validate(ServerResponseValidation)
        
        return self
    }
}
