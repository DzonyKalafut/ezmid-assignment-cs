//
//  NetworkingService.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkingService {
    private let decoder: JSONDecoder
    
    init(_ decoder: JSONDecoder = JSONDecoder()) {
        self.decoder = decoder
    }
    
    func fetchItem<T: Decodable>(using configuration: URLRequestConfigurable, ofType: T.Type, completion: @escaping (Result<T>) -> Void) {
        request(configuration, headers: [:]).validateServerResponse().responseData { response in
            switch response.result {
            case .success(let data):
                do {
                    let data = try self.decoder.decode(T.self, from: data)
                    completion(.success(data))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

fileprivate func request(_ urlRequestConfiguration: URLRequestConfigurable, headers: [String: String]) -> DataRequest {
    return Alamofire.request(
        urlRequestConfiguration.urlRequestConfiguration.url,
        method: urlRequestConfiguration.urlRequestConfiguration.method,
        parameters: urlRequestConfiguration.urlRequestConfiguration.parameters,
        encoding: urlRequestConfiguration.urlRequestConfiguration.encoding,
        headers: headers
    )
}
