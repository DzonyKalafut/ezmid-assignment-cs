//
//  URLRequestConfiguration.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Alamofire

protocol URLRequestConfigurable {
    var urlRequestConfiguration: URLRequestConfiguration { get }
}

struct URLRequestConfiguration {
    let url: Alamofire.URLConvertible
    let method: Alamofire.HTTPMethod
    let parameters: Alamofire.Parameters?
    let encoding: Alamofire.ParameterEncoding
    let headers: Alamofire.HTTPHeaders?
    
    init(url: Alamofire.URLConvertible,
         method: Alamofire.HTTPMethod = .get,
         parameters: Alamofire.Parameters? = nil,
         encoding: Alamofire.ParameterEncoding = URLEncoding.default,
         headers: Alamofire.HTTPHeaders? = nil) {
        self.url = url
        self.method = method
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
    }
}
