//
//  ArticleDetailPresenter.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

protocol ArticleDetailPresentationLogic {
    func presentArticleContent(response: ArticleDetail.LoadArticle.Response)
    func presentArticleSave(response: ArticleDetail.SaveArticle.Response)
}

class ArticleDetailPresenter: ArticleDetailPresentationLogic {
    
    weak var viewController: ArticleDetailDisplayLogic?
    
    // MARK: Presentation logic
    
    func presentArticleContent(response: ArticleDetail.LoadArticle.Response) {
        if let article = response.article {
            let articleVM = ArticleDetail.ArticleViewModel(title: article.title, content: article.content ?? "", saved: article.saveDate != nil)
            viewController?.displayArticle(viewModel: ArticleDetail.LoadArticle.ViewModel(article: articleVM))
        } else {
            viewController?.displayArticle(viewModel: ArticleDetail.LoadArticle.ViewModel(article: nil))
        }
    }
    
    func presentArticleSave(response: ArticleDetail.SaveArticle.Response) {
        viewController?.displayArticleSave(viewModel: ArticleDetail.SaveArticle.ViewModel(succes: response.success))
    }
}
