//
//  ArticleDetailRouter.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

@objc protocol ArticleDetailRoutingLogic {
}

protocol ArticleDetailDataPassing {
    var dataStore: ArticleDetailDataStore? { get }
}

class ArticleDetailRouter: NSObject, ArticleDetailRoutingLogic, ArticleDetailDataPassing {
    weak var viewController: ArticleDetailViewController?
    var dataStore: ArticleDetailDataStore?
    
    // MARK: Routing
    
    
    // MARK: Navigation

    
    // MARK: Passing data
    
}
