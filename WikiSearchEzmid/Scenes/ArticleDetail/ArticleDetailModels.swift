//
//  ArticleDetailModels.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

enum ArticleDetail {
    struct ArticleViewModel {
        let title: String
        let content: String
        let saved: Bool
    }
    
    enum LoadArticle {
        struct Request {
        }
        
        struct Response {
            let article: Article?
        }
        
        struct ViewModel {
            let article: ArticleViewModel?
        }
    }
    
    enum SaveArticle {
        struct Request {            
        }
        
        struct Response {
            let success: Bool
        }
        
        struct ViewModel {
            let succes: Bool
        }
    }
}
