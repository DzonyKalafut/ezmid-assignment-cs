//
//  ArticleDetailInteractor.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

protocol ArticleDetailBusinessLogic {
    func loadArticle(request: ArticleDetail.LoadArticle.Request)
    func saveArticle(request: ArticleDetail.SaveArticle.Request)
}

protocol ArticleDetailDataStore {
    var article: Article! { get set }
}

class ArticleDetailInteractor: ArticleDetailBusinessLogic, ArticleDetailDataStore {
    
    var presenter: ArticleDetailPresentationLogic?
    
    var article: Article!
    
    let articleWorker = WikiArticleWorker()
    
    // MARK: Business logic
    
    func loadArticle(request: ArticleDetail.LoadArticle.Request) {
        guard article.content == nil else {
            self.presenter?.presentArticleContent(response: ArticleDetail.LoadArticle.Response(article: self.article))
            return
        }
        
        articleWorker.getArticle(withPageId: article.pageId) { result in
            switch result {
            case .success(let articleDetail):
                self.article.content = articleDetail.parse.text.text
                self.presenter?.presentArticleContent(response: ArticleDetail.LoadArticle.Response(article: self.article))
            case .failure(_):
                self.presenter?.presentArticleContent(response: ArticleDetail.LoadArticle.Response(article: nil))
            }
        }
    }
    
    func saveArticle(request: ArticleDetail.SaveArticle.Request) {
        if article.realm == nil {
            article.saveDate = Date()
        }
        
        let error = RealmWorker.shared.save(object: article)
        let respose = ArticleDetail.SaveArticle.Response(success: error == nil)

        presenter?.presentArticleSave(response: respose)
    }
}
