//
//  ArticleDetailViewController.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

import UIKit
import WebKit

protocol ArticleDetailDisplayLogic: AnyObject, PopoverRenderer, ActivityIndicatorRenderer {
    func displayArticle(viewModel: ArticleDetail.LoadArticle.ViewModel)
    func displayArticleSave(viewModel: ArticleDetail.SaveArticle.ViewModel)
}

class ArticleDetailViewController: UIViewController {
    var interactor: ArticleDetailBusinessLogic?
    var router: ArticleDetailRouter?
    
    private var webView: WKWebView!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ArticleDetailInteractor()
        let presenter = ArticleDetailPresenter()
        let router = ArticleDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUIForArticleViewModel(_ viewModel: ArticleDetail.ArticleViewModel) {
        navigationItem.title = viewModel.title
        if !viewModel.saved {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "saveButtonTitle".localized, style: .done, target: self, action: #selector(saveArticle))
        }
        webView.loadHTMLString(viewModel.content, baseURL: URL(string: Config.baseUrl))
    }

    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadArticle()
    }
    
    // Set the view for more confortable zoom level possibly without horizontal scrolling
    override func loadView() {
        let source: String = "var meta = document.createElement('meta');" + "meta.name = 'viewport';" + "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" + "var head = document.getElementsByTagName('head')[0];" + "head.appendChild(meta);"
        let script: WKUserScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        let userContentController: WKUserContentController = WKUserContentController()
        let conf = WKWebViewConfiguration()
        
        conf.userContentController = userContentController
        userContentController.addUserScript(script)
        
        webView = WKWebView(frame: CGRect.zero, configuration: conf)
        view = webView
    }
    
    // MARK: Requests
    
    private func loadArticle() {
        showActivityIndicator()
        
        let request = ArticleDetail.LoadArticle.Request()
        interactor?.loadArticle(request: request)
    }
    
    @objc private func saveArticle() {
        showActivityIndicator()
        
        let request = ArticleDetail.SaveArticle.Request()
        interactor?.saveArticle(request: request)
    }
}

// MARK: Display logic

extension ArticleDetailViewController: ArticleDetailDisplayLogic {
    func displayArticle(viewModel: ArticleDetail.LoadArticle.ViewModel) {
        if let article = viewModel.article {
            setupUIForArticleViewModel(article)
        } else {
            hideActivityIndicator()
            display(errorMessage: "errorArticleFetch".localized)
        }
    }
    
    func displayArticleSave(viewModel: ArticleDetail.SaveArticle.ViewModel) {
        hideActivityIndicator()
        
        if viewModel.succes {
            navigationItem.rightBarButtonItem = nil
            display(successMessage: "articleSavedMessage".localized)
        } else {
            display(errorMessage: "errorArticleFetch".localized)
        }
    }
}

// MARK: WKNavigationDelegate

extension ArticleDetailViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideActivityIndicator()
    }
}
