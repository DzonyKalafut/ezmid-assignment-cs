//
//  ArticleListViewController.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

import UIKit

private let searchBarDefaultHeight: CGFloat = 56

protocol ArticleListDisplayLogic: AnyObject {
    func displayGetArticles(viewModel: ArticleList.GetArticles.ViewModel)
}

class ArticleListViewController: UIViewController, ErrorPopoverRenderer {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var articlesTableView: UITableView!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    
    var interactor: ArticleListBusinessLogic?
    var router: ArticleListRouter?
    
    private let throttler = Throttler(minimumDelay: 0.3)
    private var articles = [ArticleList.ArticleViewModel]()
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = ArticleListInteractor()
        let presenter = ArticleListPresenter()
        let router = ArticleListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        guard let displayType = interactor?.displayType else {
            fatalError("Interactor instance is nil")
        }
        
        switch displayType {
        case .saved:
            setupViewForSavedArticles()
        case .search:
            setupViewForArticleSearch()
        }
    }
    
    private func setupViewForSavedArticles() {
        searchBar.isHidden = true
        searchBarHeight.constant = 0
        navigationItem.title = "savedArticlesTitle".localized
        setupArticleTableView(infiniteScroll: false)
    }
    
    private func setupViewForArticleSearch() {
        searchBar.isHidden = false
        searchBarHeight.constant = searchBarDefaultHeight
        title = "articleSearchTitle".localized
        searchBar.delegate = self
        setupArticleTableView(infiniteScroll: true)
    }
    
    private func setupArticleTableView(infiniteScroll: Bool) {
        articlesTableView.delegate = self
        articlesTableView.dataSource = self
        articlesTableView.rowHeight = UITableView.automaticDimension
        articlesTableView.estimatedRowHeight = 120
        articlesTableView.register(UINib(nibName: WikiArticleResultCell.identifier, bundle: nil), forCellReuseIdentifier: WikiArticleResultCell.identifier)
        
        if infiniteScroll {
            setupInfiniteScroll()
        }
    }
    
    private func setupInfiniteScroll() {
        articlesTableView.addInfiniteScroll { [weak self] _ in
            guard let searchText = self?.searchBar.text, !searchText.isEmpty else {
                self?.articlesTableView.finishInfiniteScroll()
                return
            }
            
            self?.loadMoreArticles()
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getArticles()
        interactor?.registerForDataChangeEvents()
    }
    
    // MARK: Reuqests
    
    private func getArticles(query: String? = nil) {
        let request = ArticleList.GetArticles.Request(query: query)
        interactor?.getArticles(request: request)
    }
    
    private func loadMoreArticles() {
        let request = ArticleList.LoadMoreArticles.Request()
        interactor?.loadMoreArticles(request: request)
    }
    
    private func deleteArticle(indexPath: IndexPath) {
        let request = ArticleList.DeleteArticle.Request(row: indexPath.row)
        interactor?.deleteArticle(request: request)
    }
}

// MARK: Display logic

extension ArticleListViewController: ArticleListDisplayLogic {
    func displayGetArticles(viewModel: ArticleList.GetArticles.ViewModel) {
        self.articlesTableView.finishInfiniteScroll()
        
        if let articles = viewModel.articles {
            self.articles = articles
            articlesTableView.reloadData()
        } else {
            display(errorMessage: "errorArticleListFetch".localized)
        }
    }
}

// MARK: UITableView methods

extension ArticleListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.routeToArticleDetail()
        searchBar.resignFirstResponder()
        articlesTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let interactor = self.interactor else {
            fatalError("Interactor instance is nil")
        }
        
        return interactor.displayType == .saved
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            deleteArticle(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = articlesTableView.dequeueReusableCell(withIdentifier: WikiArticleResultCell.identifier, for: indexPath) as? WikiArticleResultCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        guard let article = articles[safe: indexPath.row] else {
            fatalError("Error while dequeuing article")
        }
        
        cell.set(article: article)
        
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}

// MARK: UISearchBarDelegate

extension ArticleListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        throttler.throttle { [weak self] in
            self?.getArticles(query: searchText)
        }
    }
}
