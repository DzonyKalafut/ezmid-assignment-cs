//
//  ArticleListModels.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

enum ArticleList {
    enum DisplayType {
        case search, saved
    }
    
    struct ArticleViewModel {
        let title: String
        let snippet: String
        let saved: Bool
    }
    
    enum GetArticles {
        struct Request {
            let query: String?
        }
        
        struct Response {
            let articles: [Article]?
        }
        
        struct ViewModel {
            let articles: [ArticleViewModel]?
        }
    }
    
    enum LoadMoreArticles {
        struct Request {
        }
    }
    
    enum DeleteArticle {
        struct Request {
            let row: Int
        }
    }
}
