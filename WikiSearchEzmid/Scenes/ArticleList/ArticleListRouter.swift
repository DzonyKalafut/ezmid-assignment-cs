//
//  ArticleListRouter.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

import UIKit

@objc protocol ArticleListRoutingLogic
{
    func routeToArticleDetail()
}

protocol ArticleListDataPassing
{
    var dataStore: ArticleListDataStore? { get }
}

class ArticleListRouter: NSObject, ArticleListRoutingLogic, ArticleListDataPassing {
    weak var viewController: ArticleListViewController?
    var dataStore: ArticleListDataStore?
    
    // MARK: Routing
    
    func routeToArticleDetail() {
        let destinationVC = ArticleDetailViewController()
        var destinationDS = destinationVC.router!.dataStore!
        passDataToArticleDetail(source: dataStore!, destination: &destinationDS)
        navigateToArticleDetail(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    
    func navigateToArticleDetail(source: ArticleListViewController, destination: ArticleDetailViewController) {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    
    func passDataToArticleDetail(source: ArticleListDataStore, destination: inout ArticleDetailDataStore) {
        let selectedRow = viewController?.articlesTableView.indexPathForSelectedRow?.row
        destination.article = source.articles[selectedRow!]
    }
}
