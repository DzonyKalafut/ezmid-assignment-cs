//
//  ArticleListInteractor.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

protocol ArticleListBusinessLogic {
    var displayType: ArticleList.DisplayType { get set }
    func registerForDataChangeEvents()
    func getArticles(request: ArticleList.GetArticles.Request)
    func loadMoreArticles(request: ArticleList.LoadMoreArticles.Request)
    func deleteArticle(request: ArticleList.DeleteArticle.Request)
}

protocol ArticleListDataStore {
    var articles: [Article] { get }
}

class ArticleListInteractor: ArticleListBusinessLogic, ArticleListDataStore {
    
    var presenter: ArticleListPresentationLogic?
    
    let articleWorker = WikiArticleWorker()
    
    var articles = [Article]()
    var displayType = ArticleList.DisplayType.search
    
    private var currentOffset = 0
    private var lastQuery = ""
    
    // MARK: Business logic
    
    func registerForDataChangeEvents() {
        RealmWorker.shared.addDelegate(delegate: self)
    }
    
    func getArticles(request: ArticleList.GetArticles.Request) {        
        currentOffset = 0
        if let query = request.query {
            lastQuery = query
        }
        
        getArticles { articles in
            self.articles = articles
            self.presenter?.presentGetArticles(response: ArticleList.GetArticles.Response(articles: self.articles))
        }
    }
    
    func loadMoreArticles(request: ArticleList.LoadMoreArticles.Request) {
        currentOffset += Config.maxArticlesToLoad
        
        getArticles { newArticles in
            self.articles += newArticles
            self.presenter?.presentGetArticles(response: ArticleList.GetArticles.Response(articles: self.articles))
        }
    }
    
    func deleteArticle(request: ArticleList.DeleteArticle.Request) {
        let article = articles[request.row]
        _ = RealmWorker.shared.remove(object: article)
    }
    
    // MARK: Private methods

    private func getArticles(successClosure: @escaping ([Article]) -> Void) {
        let savedArticles = getArticlesFromDB()
        
        switch displayType {
        case .saved:
            successClosure(savedArticles)
        case .search:
            searchForArticles(query: lastQuery) { articles in
                var foundArticles = articles
                self.markSavedArticles(source: &foundArticles, savedArticles: savedArticles)
                successClosure(foundArticles)
            }
        }
    }
    
    private func getArticlesFromDB() -> [Article] {
        let articles = RealmWorker.shared.fetchObjects(ofType: Article.self, sortedBy: "saveDate", ascending: false)
        return articles ?? []
    }
    
    private func searchForArticles(query: String, successClosure: @escaping ([Article]) -> Void) {
        guard !query.isEmpty else {
            successClosure([])
            return
        }
        
        articleWorker.getArticles(query: ArticlesQueryAPI(searchTerm: lastQuery.URLEscaped, limit: Config.maxArticlesToLoad, offset: currentOffset)) { result in
            switch result {
            case .success(let searchResult):
                let articles = searchResult.query.search.compactMap { Article(fromArticleAPI: $0) }
                successClosure(articles)
            case .failure(_):
                self.presenter?.presentGetArticles(response: ArticleList.GetArticles.Response(articles: nil))
            }
        }
    }
    
    private func refreshArticleAfterDBChange() {
        let savedArticles = getArticlesFromDB()
        
        switch displayType {
        case .saved:
            articles = savedArticles
        case .search:
            markSavedArticles(source: &articles, savedArticles: savedArticles)
        }
        
        presenter?.presentGetArticles(response: ArticleList.GetArticles.Response(articles: articles))
    }
    
    private func markSavedArticles(source: inout [Article], savedArticles: [Article]) {
        let savedPageIds = Set(savedArticles.compactMap { $0.pageId })
        source.forEach { $0.saveDate = (savedPageIds.contains($0.pageId)) ? Date() : nil }
    }
}

// MARK: RealmWorkerDelegate

extension ArticleListInteractor: RealmWorkerDelegate {
    func realmWorkerHasChanged() {
        refreshArticleAfterDBChange()
    }
}
