//
//  ArticleListPresenter.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright (c) 2019 Jan Hajnar. All rights reserved.
//

protocol ArticleListPresentationLogic {
    func presentGetArticles(response: ArticleList.GetArticles.Response)
}
class ArticleListPresenter: ArticleListPresentationLogic {
    
    weak var viewController: ArticleListDisplayLogic?
    
    // MARK: Presentation logic
    
    func presentGetArticles(response: ArticleList.GetArticles.Response) {
        let articles = response.articles?.compactMap{ articleToArticleViewModel($0) }
        viewController?.displayGetArticles(viewModel: ArticleList.GetArticles.ViewModel(articles: articles))
    }
}

fileprivate func articleToArticleViewModel(_ article: Article) -> ArticleList.ArticleViewModel {
    return ArticleList.ArticleViewModel(title: article.title, snippet: article.snippet, saved: article.saveDate != nil)
}
