//
//  WikiArticleResultCell.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import UIKit

class WikiArticleResultCell: UITableViewCell {
    static let identifier = String(describing: WikiArticleResultCell.self)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var snippetLabel: UILabel!
    @IBOutlet weak var savedLabel: UILabel!
    
    func set(article: ArticleList.ArticleViewModel) {
        titleLabel.text = article.title
        snippetLabel.attributedText = NSAttributedString(html: article.snippet)
        savedLabel.text = article.saved ? "savedCellTitle".localized : ""
    }
}

