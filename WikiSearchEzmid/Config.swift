//
//  Config.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

enum Config {
    static let baseUrl = "https://en.wikipedia.org/"
    static let apiUrl = baseUrl + "w/api.php"
    static let maxArticlesToLoad = 15
}
