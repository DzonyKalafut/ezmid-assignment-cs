//
//  ArticleDetail.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

struct ArticleDetailAPI: Decodable {
    struct Parse: Decodable {
        struct ArticleDetailText: Decodable {
            let text: String
            
            enum CodingKeys: String, CodingKey {
                case text = "*"
            }
        }
        
        let title: String
        let pageid: Int
        let text: ArticleDetailText
    }
    
    let parse: Parse
}


