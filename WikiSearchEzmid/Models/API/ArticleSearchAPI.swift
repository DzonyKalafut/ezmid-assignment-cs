//
//  ArticleSearchAPI.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

struct ArticleSearchAPI: Decodable {
    struct Query: Decodable {
        struct Article: Decodable {
            let title: String
            let pageId: Int
            let snippet: String
            
            enum CodingKeys: String, CodingKey {
                case title
                case pageId = "pageid"
                case snippet
            }
        }
        
        let search: [Article]
    }

    let query: Query
}


