//
//  ArticlesQueryAPI.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

struct ArticlesQueryAPI {
    let searchTerm: String
    let limit: Int
    let offset: Int
}
