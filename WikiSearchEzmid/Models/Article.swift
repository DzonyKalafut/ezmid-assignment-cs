//
//  Article.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 15/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation
import RealmSwift

class Article: Object {
    @objc dynamic var pageId: Int = 0
    @objc dynamic var title = ""
    @objc dynamic var snippet = ""
    @objc dynamic var content: String? = nil
    @objc dynamic var saveDate: Date? = nil
    
    override static func primaryKey() -> String? {
        return "pageId"
    }
}

extension Article {
    convenience init(fromArticleAPI article: ArticleSearchAPI.Query.Article) {
        self.init()
        
        pageId = article.pageId
        title = article.title
        snippet = article.snippet
    }
}
