//
//  ServerError.swift
//  WikiSearchEzmid
//
//  Created by Jan Hajnar on 10/04/2019.
//  Copyright © 2019 Jan Hajnar. All rights reserved.
//

import Foundation

enum ServerError: Error {
    case failureWithReason(reason: String, code: Int)
    case failure(code: Int)
}

extension ServerError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .failureWithReason(let reason, _):
            return reason
        case .failure(let code):
            return "\(code)"
        }
    }
}
